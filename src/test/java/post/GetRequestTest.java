package post;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.contains;


public class GetRequestTest {
    public final String URL = "http://localhost:3000/comments";

    @Test
    void getAllCommentsInServerWhenStatusCodeSiteIs200() {
        when().get(URL).
                then().statusCode(200);
    }

    @Test
    public void getSingleCommentInServerWhenStatusCodeSiteIs200() {
        when().get(URL + "?id=2").
                then().
                assertThat().statusCode(200).
                assertThat().body("id", contains(2));
    }
}
