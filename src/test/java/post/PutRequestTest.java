package post;

import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class PutRequestTest {
    public final String URL = "http://localhost:3000/comments";

    @Test
    void editRecordAndCheckingIfItHasBeenChanged() {

        Map<String, String> updatedComments = new HashMap<>();
        updatedComments.put("body", "ChangeBody");
        updatedComments.put("postId", "ChangePostId");
        given().
                contentType(ContentType.JSON)
                .body(updatedComments)
                .when()
                .put(URL + "/" + 3)
                .then()
                .statusCode(200)
                .body("body", equalTo("ChangeBody"))
                .body("postId", equalTo("ChangePostId"));
    }
}
