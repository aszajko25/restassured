package post;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class Delete {
    public final String URL = "http://localhost:3000/comments";

    @Test
    void deleteExistingPost() {

        given()
                .pathParam("id", "2")
                .when()
                .get(URL + "/{id}").then().statusCode(200);
        given()
                .pathParam("id", "2")
                .when()
                .delete(URL + "/{id}").then().statusCode(200);
        given()
                .pathParam("id", "2")
                .when()
                .get(URL + "/{id}").then().statusCode(404);
    }
}
