package post;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.contains;

public class PostRequestTest {
    public final String URL = "http://localhost:3000/comments";


    @Test
    void shouldReturnCode200WhenSavedNewPost() {
        Map<String, String> post = new HashMap<>();
        post.put("body", "Test");
        post.put("postId", "1Test");
        post.put("id","125");
        given()
                .contentType(ContentType.JSON)
                .body(post)
                .when()
                .post(URL)
                .then()
                .assertThat().body("id", contains(125))
                .statusCode(201);
    }

}
